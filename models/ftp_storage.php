<?php
defined("C5_EXECUTE") or die(_("Access Denied"));

class FtpStorage{
  function __construct($settings) {
    $this->settings = $settings;
    if (!(is_callable("ftp_connect"))){
      throw(new Exception(t("Your php does not have ftp functions. Please reinstall php with ftp functions")));
    }
  }


  function connect() {
    $s = $this->settings;
    $this->connection = ftp_connect($s["ftp_host"], $s["ftp_port"]);
    if (!$this->connection){
      throw(new Exception(t("Could not connect to FTP. Check your ftp server and port settings")));
    }
    $login_result = ftp_login($this->connection, $s["ftp_user"], $s["ftp_pass"]);
    if (!$login_result) {
      throw(new Exception(t("Could not login to FTP. Check your ftp login settings")));
    }
    $chdir_result = ftp_chdir($this->connection, $s["ftp_root"]);
    if (!$chdir_result){
      throw(new Exception(t("Could not find root directory on FTP. Check your ftp directory settings.")));
    }
  }

  function check_connection(){
    if ($this->connection == null) throw(new Exception(t("FTP connection not initialized")));
  }

  function upload($file) {
    $this->check_connection();
    return ftp_put($this->connection, basename($file), $file, FTP_BINARY);
   
  }

  function list_files() {
    $this->check_connection();
    return ftp_nlist($this->connection, ".");
  }

  function delete($filename) {
    $this->check_connection();
    return ftp_delete($this->connection, $filename);
  }

}
?>