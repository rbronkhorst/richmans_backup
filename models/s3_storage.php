<?php
defined("C5_EXECUTE") or die(_("Access Denied"));

class S3Storage  {
  function __construct($settings) {
    $this->settings = $settings;
    Loader::library('3rdparty/s3', 'richmans_backup');
  }


  function connect() {
    $s = $this->settings;
    $result = $this->connection = new S3($s['s3_access_key'], $s['s3_secret_key'], false, "s3-{$s['s3_region']}.amazonaws.com");

  }

  function upload($file) {
    $s = $this->settings;
    $result = $this->connection->putObject($this->connection->inputFile($file, false), $s['s3_bucket'], basename($file), S3::ACL_AUTHENTICATED_READ);
    return result;
  }

  function list_files() {
    $s = $this->settings;
    $result = $this->connection->getBucket($s['s3_bucket']);
    if ($result===false) {
      throw(new Exception(t("Could not list files")));

    }else{
      return array_keys($result);
    }
  }

  function delete($filename) {
    $s = $this->settings;
    $result = $this->connection->deleteObject($s['s3_bucket'], $filename);
    return $result;
  }
}
?>