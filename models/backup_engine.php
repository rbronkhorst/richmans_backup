<?php
defined("C5_EXECUTE") or die(_("Access Denied"));

class BackupEngine extends Object {
	var $default_settings = array(
		"tmp_dir" =>  "files/tmp/backup",
		"zip" => "/usr/bin/zip",
		"backup_files" => "*",
		"exclude_dirs" => array("files/tmp/*"),
		"backup_prefix"=> "backup_",
		"destination" => "FTP",
		"s3_secret_key" => "",
		"s3_access_key" => "",
		"s3_bucket" => "",
		"s3_region" => "s3",
		"ftp_host" => "localhost",
		"ftp_port" => 21,
		"ftp_user" => "backup_test",
		"ftp_pass" => "test1234",
		"ftp_root" => "/",
		"keep_backups" => 3,
		"root_dir" => ".",
		"debug" => false
	);
	var $mysql_dump_file = NULL;
	var $backup_file = NULL;
	var $settings;
	function __construct($options=array()) {
		$this->settings = array_merge($this->default_settings, $options);

	}
	/*
		Prints  a debug message if debugging is enabled.
		Also shows the function and line where this function was called
		As the name suggests, this is used only for debugging, which can be
		enabled in the settings above. To see the output, right click the
		play button in the automated jobs screen, and click open in new tab
	*/
	function debug($message,$offset=1){
		$debug = $this->settings['debug'];
		if (!$debug) return;
		
		$trace=debug_backtrace();
		$function=str_pad($trace[$offset]["function"],15);
		$line = $trace[$offset-1]["line"];
		$line=str_pad($line,4,"0",STR_PAD_LEFT);
		$function=str_replace(" ","&nbsp;",$function);
		echo ("<span style='font-family:courier;font-weight:900'>"
				."[$function ($line)]  </span>$message<br/>\n");
		flush();
		if (ob_get_level()) ob_flush();
	}
	
	function fail($message) {
		$this->clean_up_local();
		throw(new Exception($message));
	}
	
	function go_to_root(){
		if (chdir($this->settings["root_dir"])) {
			$this->debug("Entered root directory");
		} else {
			$this->debug("Can't find root dir");
			$this->fail(t("no root dir"));
		}
	}
	function check_tmp_dir(){
		if (is_dir($this->settings["tmp_dir"])){
			$result = TRUE;
		} else {
			$result = mkdir($this->settings["tmp_dir"], 0700, true);
		}
		if ($result === FALSE){
			$this->debug("Could not create temp dir!");
			$this->fail(t("No temp dir"));
		}
		$this->debug("Temp dir is ready for action");
	}



	function execute_command($cmd, $input=''){

		$proc=proc_open($cmd, array(0=>array('pipe', 'r'), 1=>array('pipe', 'w'), 2=>array('pipe', 'w')), $pipes); 
		fwrite($pipes[0], $input);fclose($pipes[0]); 
		$stdout=stream_get_contents($pipes[1]);fclose($pipes[1]); 
		$stderr=stream_get_contents($pipes[2]);fclose($pipes[2]); 
		$rtn=proc_close($proc); 
		return array('stdout'=>$stdout, 
			'stderr'=>$stderr, 
			'return'=>$rtn 
		); 
	}
	
	/*
	 Execute a program and die if it fails
	*/
	function execute_program($command, $dump_stdout=FALSE){
		$result = $this->execute_command($command);
		if ($result['stderr'] != "") $this->debug($result["stderr"]);
		if (($result['stdout'] != "") && ($dump_stdout)) $this->debug($result["stdout"]);
		if ($result['return'] != 0) {
			$this->fail("Failed command: {$result['stderr']}");
		}
	}


	/*
	 Create a dump of the database and store it on local disk
	*/
	function dump_mysql() {
		$s = $this->settings;
		
		Loader::library("backup");
		$backup = Backup::execute(false); 
		/* Find the generated backup file, which doesnt get returned from the backup... sigh */
		$fh = Loader::helper('file');
		$backup_files = @$fh->getDirectoryContents(DIR_FILES_BACKUPS);
		if (count($backup_files) < 0) $this->fail(t("Could not backup database"));
		
		rsort($backup_files);

		$this->mysql_dump_file = DIR_FILES_BACKUPS . "/" . $backup_files[0];
		$this->debug("Found db backup in ". $this->mysql_dump_file);
		chmod($this->mysql_dump_file,777);
		return $this->mysql_dump_file;
	}

	/*
	Create a nice packed file of all the data that needs to backup
	*/
	function pack_files() {
		$s = $this->settings;
		$this->backup_file = "{$s["tmp_dir"]}/" . $s["backup_prefix"] . date("YmdHis") . ".zip";
		$excludes = '';
		foreach($s['exclude_dirs'] as $exclude_dir) {
			$excludes .= "'{$exclude_dir}/*' ";

		}
		
		$command = "{$s["zip"]} -r $this->backup_file ";
		$command .= "{$s["backup_files"]} -x {$excludes}";
		$this->debug($command);
		$this->execute_program($command);
		
		# add the mysql backup
		$command = "{$s["zip"]} -j -r $this->backup_file ";		
		$command .= "$this->mysql_dump_file";

		$this->execute_program($command);
	}
	
	/*
	 Make a connection to the storage where we are storing all backups
	*/
	function connect_to_storage() {
		if($this->settings['destination'] == 'FTP'){ 
			Loader::model('ftp_storage', 'richmans_backup');
			$this->storage = new FtpStorage($this->settings);
		} else {
			Loader::model('s3_storage', 'richmans_backup');
			$this->storage = new S3Storage($this->settings);
		}
		$this->storage->connect();

	}

	/*
	 Deletes one or more files from the storage to make place for the new
	 backup.
	*/
	function clean_up_storage() {
		$contents = $this->storage->list_files();
		$contents = array_diff($contents, array(".", ".."));
		$backups = array();
		$prefix = $this->settings['backup_prefix'];
		foreach($contents as $file) {
			if (substr($file, 0, strlen($prefix)) == $prefix) {
				array_push($backups, $file);
			}
		}
		$backup_count = count($backups);
		$this->debug("Found $backup_count backups");
		$delete_backups = ($backup_count - $this->settings["keep_backups"]) + 1;
		if($delete_backups <= 0) return;
		
		sort($backups);
		for($i=0; $i < $delete_backups; $i++){
			$filename = $backups[$i];
			$this->debug("Deleting $filename");
			$delete_result = $this->storage->delete($filename);
			if (!$delete_result) {
				$this->fail(t("Could not delete ") . $filename);
			} else {
				$this->debug("Deleted $filename");
			}
		}
	}


	/*
	 Upload the backup file to storage
	*/
	function upload_file() {
		$put_result = $this->storage->upload($this->backup_file);
		if(!$put_result) {
      $this->fail(t("Could not upload file"));
    } else {
      $this->debug("Successfuly uploaded file");
    }
	}
	
	/*
		After uploading, clean up the local copy
	*/
	function clean_up_local(){
		if ($this->mysql_dump_file != NULL) unlink($this->mysql_dump_file);
		if ($this->backup_file != NULL) unlink($this->backup_file);
		
	}
	/*
	 This is where it all happens.
	*/
	public function run() {
		set_time_limit(0);
		ini_set('max_execution_time', 0);
		$this->debug("Richmans Backup Engine v1.0");
		$this->go_to_root();
		$this->check_tmp_dir();
		$this->debug("Starting mysqldump");
		$this->dump_mysql();
		$this->debug("Packing up files");
		$this->pack_files();
		$this->debug("Connecting to storage");
		$this->connect_to_storage();
		$this->debug("Cleaning up storage");
		$this->clean_up_storage();
		$this->debug("Uploading file");
		$this->upload_file();
		$this->debug("Deleting local file");
		$this->clean_up_local();
		$this->debug("All done! Thank you come again!");
		return t("Backup finished");
	}
}

?>