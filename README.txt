=== Richmans Backup ===
This concrete5 plugin allows you to create backups and send them to a different server.
You can choose to store backups on amazon s3, or an ftp server.

==== Installation ====
Copy the richmans_backup folder to the packages folder of your concrete installation
In dashboard->install addons, click install
In dashboard->system->clear cache, clear the cache

=== Amazon S3 Setup ===
To backup to amazon s3, you need to setup a user that has access to a bucket.

First, use the s3 control panel to create a bucket. Make sure you note which region you put it in.

Next, head over to the 'Identity and Access Management (IAM)'. 
In IAM, you need to create a user account. It does not need a password, but you need to generate 
credentials (Access key and secret key).

Lastly, you need to give the user 2 policies.

To list the bucket contents:
Choose the permissions tab and click attach user policy
Click policy generator and select
Chose amazon s3 for aws service
Select the action ListBucket
For arn type: arn:aws:s3:::your_bucket_name, and click add statement
Click continue and then apply policy

To add and delete objects:
Choose the permissions tab and click attach user policy
Click policy generator and select
Chose amazon s3 for aws service
Select the actions: PutObject, PutObjectAcl and DeleteObject
For arn type: arn:aws:s3:::your_bucket_name/*, and click add statement
Click continue and then apply policy

=== Scheduling the job ===
Richmans_backup is installed as an 'automated job'. If you want this job to run automatically, 
you should have a system that calls the url at the bottom of the automated jobs page periodically. 
The easiest way to do this is to use cron on your server. If you do not have access to cron, you can 
google for 'online cron' to find some sites that will do that for you.