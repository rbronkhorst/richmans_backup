<?php
defined("C5_EXECUTE") or die(_("Access Denied"));
    $ih = Loader::helper('concrete/interface'); 
    $form = Loader::helper('form');
		 Loader::library('3rdparty/s3', 'richmans_backup');
?>

<?php
if ($compatibility_mode == true){
?>
<!-- Some css copied from concrete5.6 to make this view reasonable for concrete5.5 -->
<style>
.ccm-ui fieldset legend { padding-left: 0;}

div.alert{
	padding: 8px 35px 8px 14px;
	margin-bottom: 18px;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	border: 1px solid #FBEED5;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}

div.alert-info {
	background-color: #D9EDF7;
	border-color: #BCE8F1;
	color: #3A87AD;
}

div.alert-error {
	background-color: #D9EDF7;
	border-color: #BCE8F1;
	color: #3A87AD;
}

.ccm-ui .form-horizontal .control-group {
	margin-bottom: 18px;
}

.ccm-ui .form-horizontal .controls {
	margin-left: 160px;
}

.ccm-ui label {
	display: block;
	margin-bottom: 5px;
	color: #333;
}

.ccm-ui label.radio{
	float:none;
	width:auto;
	text-align:left;
}
</style>


<?php } ?>
<script type='text/javascript'>
 function richmans_backup_check_destination(){
 	 var destination = $("input[name='RICHMANS_BACKUP_DESTINATION']:checked").val();
 	 if (destination === 'FTP') {
 	 		$('div#ftp-settings').show(500);
 	 		$('div#s3-settings').hide(500);
 	 		
 	 } else {
			$('div#ftp-settings').hide(500);
 	 		$('div#s3-settings').show(500);
 	 		
 	 }
 };

 $(function(){
 	 richmans_backup_check_destination();
 	 $("input[name='RICHMANS_BACKUP_DESTINATION']").change(function(){
 			richmans_backup_check_destination();
 		})
 })
</script>
<div class="ccm-ui">
	<div class="row">
		<?php if ($notice != ""){ ?>
				<div class="alert alert-info"><?php echo $notice?> </div>
		<?php } ?>
		<?php if ($ftp_functions_found != TRUE) { ?>
			<div class="alert alert-error"><?php echo t('PHP functions not found! Please enable ftp in your php installation. See')?> <a href='http://www.php.net/manual/en/ftp.installation.php'><?php echo t('This page')?></a> <?php echo t('for more info.')?></div>
		<?php }?>
		<div class="ccm-pane">
		      <?php echo Loader::helper('concrete/dashboard')->
getDashboardPaneHeader(t('Richmans Backup Settings'), array(t('Use this window to configure the automatic backup job'))); ?>
						<form method="post" action="<?php echo $this->action('update_settings')?>" class="form-horizontal" id="backup-settings-form">
								
            <div class="ccm-pane-body">
								
								<fieldset>
										<legend><?php echo t('Backup source')?></legend>
										<div class='alert alert-info'>
												<?php echo t('The following paths are excluded from the backup:')?>
												<ul>
														<li>/files/cache</li>
														<li>/files/tmp</li>
														<li>/files/trash</li>
														<li>/files/backup</li>
														<li>/config/site.php</li>
												</ul>
										</div>
										<div class="control-group">
												<?php echo $form->label("RICHMANS_BACKUP_SOURCE", t("What to backup: "));?>
												<div class="controls">
														<label class="radio"><?php echo $form->radio('RICHMANS_BACKUP_SOURCE', 'EVERYTHING', Config::get('RICHMANS_BACKUP_SOURCE'))?> <span><?php echo t('The database and the complete concrete5 installation')?></span></label>
														<label class="radio"><?php echo $form->radio('RICHMANS_BACKUP_SOURCE', 'JUST_FILES', Config::get('RICHMANS_BACKUP_SOURCE'))?> <span><?php echo t('The database and uploaded files')?></span></label>
												</div>
										</div>
										
								</fieldset>
								<fieldset>
										<legend><?php echo t('Backup to')?></legend>
										<div class="control-group">
												<?php echo $form->label("RICHMANS_BACKUP_DESTINATION", t("Upload backups to: "));?>
												<div class="controls">
														<label class="radio"><?php echo $form->radio('RICHMANS_BACKUP_DESTINATION', 'FTP', Config::get('RICHMANS_BACKUP_DESTINATION'))?> <span><?php echo t('FTP Server')?></span></label>
														<label class="radio"><?php echo $form->radio('RICHMANS_BACKUP_DESTINATION', 'S3', Config::get('RICHMANS_BACKUP_DESTINATION'))?> <span><?php echo t('Amazon S3')?></span></label>
												</div>
										</div>

										<div id='ftp-settings'>
											<div class="control-group">
													<?php echo $form->label('RICHMANS_BACKUP_FTP_SERVER',t('Server'));?>
													<div class="controls">
														<?php echo $form->text('RICHMANS_BACKUP_FTP_SERVER', Config::get('RICHMANS_BACKUP_FTP_SERVER'))?>
													</div>
											</div>
											<div class="control-group">
													<?php echo $form->label('RICHMANS_BACKUP_FTP_PORT',t('Port'));?>
													<div class="controls">
														<?php echo $form->text('RICHMANS_BACKUP_FTP_PORT', Config::get('RICHMANS_BACKUP_FTP_PORT'))?>
													</div>
											</div>
											<div class="control-group">
													<?php echo $form->label('RICHMANS_BACKUP_FTP_USERNAME',t('Username'));?>
													<div class="controls">
														<?php echo $form->text('RICHMANS_BACKUP_FTP_USERNAME', Config::get('RICHMANS_BACKUP_FTP_USERNAME'))?> 
													</div>
											</div>
											
											<div class="control-group">
													<?php echo $form->label('RICHMANS_BACKUP_FTP_PASSWORD',t('Password'));?>
													<div class="controls">
														<?php echo $form->password('RICHMANS_BACKUP_FTP_PASSWORD', '')?> (<?php echo t('Leave empty to keep current password');?>)
													</div>
											</div>
											
											<div class="control-group">
													<?php echo $form->label('RICHMANS_BACKUP_FTP_DIRECTORY',t('Directory'));?>
													<div class="controls">
														<?php echo $form->text('RICHMANS_BACKUP_FTP_DIRECTORY', Config::get('RICHMANS_BACKUP_FTP_DIRECTORY'))?>
													</div>
											</div>
										</div>
										<div id='s3-settings'>
											<div class="control-group">
													<?php echo $form->label('RICHMANS_BACKUP_S3_ACCESS_KEY',t('Aws Access Key'));?>
													<div class="controls">
														<?php echo $form->text('RICHMANS_BACKUP_S3_ACCESS_KEY', Config::get('RICHMANS_BACKUP_S3_ACCESS_KEY'))?>
													</div>
											</div>

											<div class="control-group">
													<?php echo $form->label('RICHMANS_BACKUP_S3_SECRET_KEY',t('Aws Secret Key'));?>
													<div class="controls">
														<?php echo $form->text('RICHMANS_BACKUP_S3_SECRET_KEY', Config::get('RICHMANS_BACKUP_S3_SECRET_KEY'))?>
													</div>
											</div>

											<div class="control-group">
													<?php echo $form->label('RICHMANS_BACKUP_S3_BUCKET',t('Aws Bucket'));?>
													<div class="controls">
														<?php echo $form->text('RICHMANS_BACKUP_S3_BUCKET', Config::get('RICHMANS_BACKUP_S3_BUCKET'))?>
													</div>
											</div>

											<div class="control-group">
													<?php echo $form->label('RICHMANS_BACKUP_S3_REGION',t('Aws Region'));?>
													<div class="controls">
														<?php echo $form->select('RICHMANS_BACKUP_S3_REGION', S3::$regions, Config::get('RICHMANS_BACKUP_S3_REGION'))?>
														
													</div>
											</div>
										</div>
								</fieldset>
								<fieldset>
										<legend><?php echo t('Process')?></legend>
										<div class="control-group">
												<?php echo $form->label('RICHMANS_BACKUP_ZIP',t('Zip executable'));?>
												<div class="controls">
													<?php echo $form->text('RICHMANS_BACKUP_ZIP', Config::get('RICHMANS_BACKUP_ZIP'))?> 
												</div>
										</div>
										<div class="control-group">
												<?php echo $form->label('RICHMANS_BACKUP_TEMPDIR',t('Temporary folder'));?>
												<div class="controls">
													<?php echo $form->text('RICHMANS_BACKUP_TEMPDIR', Config::get('RICHMANS_BACKUP_TEMPDIR'))?> 
												</div>
										</div>
										<div class="control-group">
											<?php echo $form->label('RICHMANS_BACKUP_KEEP_BACKUPS',t('Keep backups'));?>
											<div class="controls">
												<?php echo $form->text('RICHMANS_BACKUP_KEEP_BACKUPS', Config::get('RICHMANS_BACKUP_KEEP_BACKUPS'))?>
											</div>
									</div>
								</fieldset>
						</div>
						
            <div class="ccm-pane-footer">
                <?php
                    echo $ih->submit(t("Save"), 'form', 'right', 'primary');
                ?>
            </div>
						</form>
				</div>
    </div>
</div>