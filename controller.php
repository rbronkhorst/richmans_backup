<?php
defined("C5_EXECUTE") or die(_("Access Denied"));

class RichmansBackupPackage extends Package {

     protected $pkgHandle = 'richmans_backup';
     protected $appVersionRequired = '5.5.0';
     protected $pkgVersion = '0.9.11';

     public function getPackageDescription() {
          return t("Backups the database, the files directory and uploads the backups to ftp");
     }

     public function getPackageName() {
          return t("Richmans Backup");
     }
     
     private function loadDefaultConfig() {
          Config::save('RICHMANS_BACKUP_TEMPDIR', 'files/tmp/backup');
          Config::save('RICHMANS_BACKUP_ZIP', '/usr/bin/zip');
          Config::save('RICHMANS_BACKUP_SOURCE', 'EVERYTHING');
          Config::save('RICHMANS_BACKUP_FTP_SERVER', 'ftp.myserver.com');
          Config::save('RICHMANS_BACKUP_FTP_PORT', 21);
          Config::save('RICHMANS_BACKUP_FTP_USERNAME', 'anonymous');
          Config::save('RICHMANS_BACKUP_FTP_PASSWORD', '');
          Config::save('RICHMANS_BACKUP_FTP_DIRECTORY', '/');
          Config::save('RICHMANS_BACKUP_KEEP_BACKUPS', 5);
          Config::save('RICHMANS_BACKUP_S3_ACCESS_KEY', 'Your aws key');
          Config::save('RICHMANS_BACKUP_S3_SECRET_KEY', 'Your secret key');
          Config::save('RICHMANS_BACKUP_S3_BUCKET', 'Your bucket name');
          Config::save('RICHMANS_BACKUP_S3_REGION', 's3');
          
          Config::save('RICHMANS_BACKUP_DESTINATION', 'FTP');

     }
     
     public function install() {
          $pkg = parent::install();
          Loader::model('single_page');
          Loader::model("job");
          $this->loadDefaultConfig();  
          $singlePage = SinglePage::add('/dashboard/system/backup_restore/richmans_backup', $pkg);
          $singlePage->update(array('cName' => t('Richmans Backup'), 'cDescription' => t('Your complete backup solution')));
          Job::installByPackage("do_backup", $pkg);
     }
     
}
?>
