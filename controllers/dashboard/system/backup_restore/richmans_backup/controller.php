<?php
defined("C5_EXECUTE") or die(_("Access Denied"));

class DashboardSystemBackupRestoreRichmansBackupController extends Controller {
	public function compatibility_mode() {
		$version = Config::get("SITE_APP_VERSION");
		$version_array = explode(".", $version);
		
		if (intval($version_array[1]) < 6) {

			return true;
		}else{
			return false;
		}
	}
	 

	public function view() {
		if($this->get("notice") == 'updated'){
			$this->set("notice", t("Backup settings have been updated"));
		}
		$this->set("ftp_functions_found", is_callable('ftp_connect'));
		$this->set("compatibility_mode", $this->compatibility_mode());
		
	}
	
	public function update_settings() {
		
		Config::save('RICHMANS_BACKUP_TEMPDIR', $this->post('RICHMANS_BACKUP_TEMPDIR'));
		Config::save('RICHMANS_BACKUP_ZIP', $this->post('RICHMANS_BACKUP_ZIP'));
		
		Config::save('RICHMANS_BACKUP_SOURCE', $this->post('RICHMANS_BACKUP_SOURCE'));
		Config::save('RICHMANS_BACKUP_DESTINATION', $this->post('RICHMANS_BACKUP_DESTINATION'));
		
		Config::save('RICHMANS_BACKUP_S3_ACCESS_KEY', $this->post('RICHMANS_BACKUP_S3_ACCESS_KEY'));
		Config::save('RICHMANS_BACKUP_S3_SECRET_KEY', $this->post('RICHMANS_BACKUP_S3_SECRET_KEY'));
		Config::save('RICHMANS_BACKUP_S3_BUCKET', $this->post('RICHMANS_BACKUP_S3_BUCKET'));
		Config::save('RICHMANS_BACKUP_S3_REGION', $this->post('RICHMANS_BACKUP_S3_REGION'));


		Config::save('RICHMANS_BACKUP_FTP_SERVER', $this->post('RICHMANS_BACKUP_FTP_SERVER'));
		Config::save('RICHMANS_BACKUP_FTP_PORT', $this->post('RICHMANS_BACKUP_FTP_PORT'));
		Config::save('RICHMANS_BACKUP_FTP_USERNAME', $this->post('RICHMANS_BACKUP_FTP_USERNAME'));
		if ($this->post('RICHMANS_BACKUP_FTP_PASSWORD') != ''){
			Config::save('RICHMANS_BACKUP_FTP_PASSWORD', $this->post('RICHMANS_BACKUP_FTP_PASSWORD'));
		}
		Config::save('RICHMANS_BACKUP_FTP_DIRECTORY', $this->post('RICHMANS_BACKUP_FTP_DIRECTORY'));
		Config::save('RICHMANS_BACKUP_KEEP_BACKUPS', $this->post('RICHMANS_BACKUP_KEEP_BACKUPS'));
		
		$this->redirect("/dashboard/system/backup_restore/richmans_backup?notice=updated");
	}

}
?>