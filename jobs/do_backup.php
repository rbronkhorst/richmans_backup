<?php
defined("C5_EXECUTE") or die(_("Access Denied"));

class DoBackup extends Job {
  public function getJobName() {
    return t("Richmans Backup Job");
  }
  
  public function getJobDescription() {
    return t("This job does backups for the richmans backups plugin. To configure it go to dashboard > system & settings > richmans backup.");
  }
  
  private function option_array() {
    $options = array(
          "tmp_dir" => Config::get('RICHMANS_BACKUP_TEMPDIR'),
          "ftp_host" => Config::get('RICHMANS_BACKUP_FTP_SERVER'),
          "ftp_port" => Config::get('RICHMANS_BACKUP_FTP_PORT'),
          "ftp_user" => Config::get('RICHMANS_BACKUP_FTP_USERNAME'),
          "ftp_pass" => Config::get('RICHMANS_BACKUP_FTP_PASSWORD'),
          "ftp_root" => Config::get('RICHMANS_BACKUP_FTP_DIRECTORY'),
          "destination" => Config::get('RICHMANS_BACKUP_DESTINATION'),
          "s3_access_key" => Config::get('RICHMANS_BACKUP_S3_ACCESS_KEY'),
          "s3_secret_key" => Config::get('RICHMANS_BACKUP_S3_SECRET_KEY'),
          "s3_bucket" => Config::get('RICHMANS_BACKUP_S3_BUCKET'),
          "s3_region" =>Config::get('RICHMANS_BACKUP_S3_REGION'),
          "keep_backups" => Config::get('RICHMANS_BACKUP_KEEP_BACKUPS'),
          "zip" => Config::get('RICHMANS_BACKUP_ZIP'),
          
    );
    if (Config::get('RICHMANS_BACKUP_SOURCE') == "EVERYTHING"){
      $options['backup_files'] = '.';
      $options['exclude_dirs'] = array('files/tmp', 'files/cache', 'files/trash', 'config/site.php', 'files/backup');
      $options["root_dir"] = dirname(dirname(dirname(dirname(__FILE__))));
    }else{
      $options['backup_files'] = 'files';
      $options['exclude_dirs'] = array('files/tmp', 'files/cache', 'files/trash', 'files/backup');
      $options["root_dir"] = dirname(dirname(dirname(dirname(__FILE__))));
    }
    return $options;
  }
  
  public function run() {
    Loader::model('backup_engine', 'richmans_backup');
    $backup = new BackupEngine($this->option_array());
    return $backup->run();
  }
}
?>